# Learning [ClickHouse](https://clickhouse.tech/)

> ClickHouse® is a fast open-source OLAP database management system
> It is column-oriented and allows to generate analytical reports using SQL queries in real-time.

✓ Blazing fast
✓ Linearly scalable
✓ Feature-rich
✓ Hardware efficient
✓ Fault-tolerant
✓ Highly reliable

## Action

Start the DB server: `./start-server.sh`

Start the client: `./console.sh`

## Resources

[Official docker image](https://hub.docker.com/r/yandex/clickhouse-server)
[Official playground](https://play.clickhouse.tech/?file=welcome)
[Official tutorial](https://clickhouse.tech/docs/en/getting-started/tutorial/)
[Official Go client](https://github.com/ClickHouse/clickhouse-go)
