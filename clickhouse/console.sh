#! /usr/bin/env bash

# unofficial strict mode
set -eo pipefail

SERVER_NAME="clickhouse"
CLICKHOUSE_VERSION="21.7"

docker run -i --rm \
  --link ${SERVER_NAME}:clickhouse-server \
  --volume "$PWD/init.d://opt/init.d" \
  yandex/clickhouse-client:${CLICKHOUSE_VERSION} --host clickhouse-server "$@"
