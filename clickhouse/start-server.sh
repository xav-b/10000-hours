#! /usr/bin/env bash

# unofficial strict mode
set -eo pipefail

CLICKHOUSE_VERSION="21.7"

docker run --detach \
  --name clickhouse \
  -p 9000:9000 -p 8123:8123 \
  --ulimit nofile=262144:262144 \
  "yandex/clickhouse-server:&{CLICKHOUSE_VERSION}"
