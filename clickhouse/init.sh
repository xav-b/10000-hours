#! /usr/bin/env bash

# unofficial strict mode
set -eo pipefail

DATASET_URL="https://datasets.clickhouse.tech/visits/tsv/visits_v1.tsv.xz"
curl "$DATASET_URL" | unxz --threads=`nproc` > visits_v1.tsv

 ./console.sh --query "CREATE DATABASE IF NOT EXISTS tutorial"

 ./console.sh --queries-file /opt/init.d/01-init-hits.sql --database tutorial
 ./console.sh --queries-file /opt/init.d/02-init-visits.sql --database tutorial

 ./console.sh --query "INSERT INTO tutorial.hits_v1 FORMAT TSV" --max_insert_block_size=100000 < hits_v1.tsv
 ./console.sh --query "INSERT INTO tutorial.visits_v1 FORMAT TSV" --max_insert_block_size=100000 < visits_v1.tsv
