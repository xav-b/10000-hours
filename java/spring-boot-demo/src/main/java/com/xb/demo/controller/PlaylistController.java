package com.xb.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.xb.demo.model.Playlist;
import com.xb.demo.model.Song;
import com.xb.demo.service.PlaylistService;

import java.math.BigInteger;
import java.util.Optional;

@RestController
@RequestMapping("/playlists")
public class PlaylistController {

    @Autowired
    public PlaylistService service;

    @GetMapping("/_ready")
    public String root() {
        return "ok";
    }

    @GetMapping("/")
    public Iterable<Playlist> getAllPlaylists() {
        return service.getAllPlaylists();
    }

    @GetMapping("/{id}")
    public Playlist getPlaylistById(final @PathVariable("id") BigInteger playlistId) {
        return service.getPlaylistById(playlistId);
    }

    @PostMapping("/{name}")
    public Optional<Playlist> createPlaylist(final @PathVariable String name) {
        return service.createPlaylist(name);
    }

    @PostMapping("/{id}/add")
    public Song addSongToPlaylist(final @PathVariable("id") BigInteger playlistId,
                                  final @RequestBody Song song) {
        return service.addSong(playlistId, song);
    }

    @GetMapping("/songs")
    public Iterable<Song> getAllSongs() {
        return service.getSongs(null);
    }

}
