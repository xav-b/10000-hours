# Spring Demo

> Simple RESTFul application illustrating Spring basics.

It is based on [Educative.io course](https://www.educative.io/courses/developing-microservices-with-spring-boot)
but skipping the frontend rendering entirely.

## Usage

Run the API: `./gradlew clean build bootRun`.

Then `curl localhost:9090`.