# Learning Java for Backend

From scratch to high scale APIs and sitributed systems.

Motivation: Most large companies and keystone projects (Android, Kafka,
Zookeeper, ...) are built in Java. This is still the most-used language
and even if it is less and less appreciated, Kotlin and Scala stepped-in
to make it efficient and productive for niche usage.

## Installation

**TODO**

**Runtime**:

```sh
$ java -version
java version "1.8.0_221"
Java(TM) SE Runtime Environment (build 1.8.0_221-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.221-b11, mixed mode)

$ javac -version
javac 1.8.0_221
```

## Usage

```sh
# compile
$ javac handson/Main.java
# run
$ java handson.Main
```

## Resources

- [ ] [learn Java from scratch](https://www.educative.io/courses/learn-java-from-scratch)
- [ ] [Learn Object-Oriented Programming in Java](https://www.educative.io/courses/learn-object-oriented-programming-in-java)
