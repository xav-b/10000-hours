package handson.oop;

class Crypto {
  private String token;

  public Crypto() {
    token = "j2kjn35hb3j333v";
  }

  public void wallet() {
    System.out.println("Crypto wallet token: " + token);
  }
}

class DogeCoin extends Crypto {

  public DogeCoin() {
    super();
  }

}

public class Main
{
	public static void main(String args[]) {
	  DogeCoin coin = new DogeCoin();
	    coin.wallet();
	}
}

