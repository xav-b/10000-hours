// match it to the directory structure so that imports and compilation work
// smoothly
package handson.basics;

import java.util.Scanner;
// Everything from `java.lang package` is made available without import
// Like the `Math` class


class Car {
  private int id;
  // because it is static, it is shared across classes instance
  private static int nextId;
  private String model;
  private int horsepower;

  public Car(String m, int hp) {
        this.model = m;
        this.horsepower = hp;
        id = nextId;
        nextId += 1;
    }

    // This method shows the horsepower of the car
    public void showDetails() {
        System.out.println(id + " | Car horsepower is: " + this.horsepower);
    }
}

// The classname must match the filename
class Main {
  private static void variables() {
    // A short type is a 16 bit signed integer
    short age = 32;
    // If we have to assign a value to float, we must add an ‘f’ at the end of the number
    float radius = 8.56f, perimeter = 4.5f;
    boolean part_time = false;

    char single_quote = 'N';
    String multi_quote = "Hello World";
    String[] greetings = multi_quote.split(",");
  }

  public static void askName() {
    Scanner question = new Scanner(System.in);

    System.out.println("Enter your name: ");
    String name = question.nextLine();
    System.out.println("Your name is: " + name);
  }

  public static double exerciseMath(double x, double y, double z) {
    return Math.cbrt(Math.pow(x, 2) + Math.pow(y, 2) - Math.abs(z));
  }

  public static String conditional(int i) {
    // switch
    switch (i) {
      case 0:
        System.out.println("ZERO");
        break;
      default:
        System.out.println("Not zero: " + i);
    }

    return (i > 0) ? "postive" : "negative";
  }

  // staic method
  public static void loop(int number) {
    do {
      number++;
    } while(number < 5);
  }

  public static void main(String args[]) {
    System.out.println("Exercise one: " + exerciseMath(1.2, 5.4, 7.2));

    Car panda = new Car("Fiat", 1000);
    panda.showDetails();
    Car punto = new Car("Ferrari", 100000);
    punto.showDetails();
  }
}
